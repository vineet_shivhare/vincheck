#!/bin/bash

echo "404 error count "
grep "^.*:80"  $1 | awk '{ print $10 }' | grep 404 | sort | uniq -c

echo "all HTTP responce with count "
grep "^.*:80"  $1 | awk '{ print $10 }'  | sort | uniq -c
