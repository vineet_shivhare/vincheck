#!/usr/bin/ruby


require 'fileutils'



if ( ARGV.length >= 2 )
     puts "rtt",
     port=ARGV[0],
     git_repo=ARGV[1]
     git_project=git_repo.split('/')[-1].split('.')[0]
else 
     puts "agrgument not proper"
     exit

end

vagrant="# -*- mode: ruby -*-
# vi: set ft=ruby :


Vagrant.configure('2') do |v|
  v.vm.box = \"puppetlabs\"
  v.vm.box_url =\"http://files.vagrantup.com/precise64.box\"
  #v.vm.network :private_network, ip: \"192.168.33.10\"
  v.vm.network \"forwarded_port\", guest: 80, host: #{port}
  
  #v.vm.synced_folder \"./vv\", \"/vagrant\"
  v.ssh.shell = \"bash -c 'BASH_ENV=/etc/profile exec bash'\"
  v.vm.provision :shell do |s|
    s.path = 'vagrant/bootstart.sh'
  end
  v.vm.synced_folder '.', '/home/vagrant/'
end "

#puts va


bootstart="#!/bin/bash -x

export DEBIAN_FRONTEND=noninteractive

apt-get update
apt-get -y install git curl  libcurl4-openssl-dev nodejs  --fix-missing

su -c \"source /home/vagrant/vagrant/vagrant_kickstart_conf.sh\" vagrant"

vagrant_kickstart_conf="#!/bin/bash -x

echo \"export EDITOR=vim\" >> $HOME/.bashrc

# RVM
echo \"rvm_install_on_use_flag=1\" >> $HOME/.rvmrc
echo \"rvm_project_rvmrc=1\"       >> $HOME/.rvmrc
echo \"rvm_trust_rvmrcs_flag=1\"   >> $HOME/.rvmrc
curl -L https://get.rvm.io | bash -s stable --autolibs=4
source \"$HOME/.rvm/scripts/rvm\"
[[ -s \"$rvm_path/hooks/after_cd_bundle\" ]] && chmod +x $rvm_path/hooks/after_cd_bundle
rvm autolibs enable
rvm requirements
rvm reload

_RUBY_VERSION=\"ruby-1.9.3\"
rvm install $_RUBY_VERSION
rvm gemset create myapp
rvm use $_RUBY_VERSION --default


gem update --system

echo \"gem: --no-rdoc --no-ri\" >> ~/.gemrc 

gem install rails
rvm 1.9.3 --passenger
#rvm 1.9.2
gem install passenger 

#apt-get -y install libcurl4-openssl-dev



rvmsudo `which passenger-install-nginx-module` --prefix=/etc/nginx --auto --auto-download #--extra-configure-flags='--sbin-path=/usr/sbin/nginx --conf-path=/etc/nginx/nginx.conf --error-log-path=/var/log/nginx/error.log --http-log-path=/var/log/nginx/access.log --pid-path=/var/run/nginx.pid --lock-path=/var/run/nginx.lock --user=nginx --group=nginx --with-mail --with-mail_ssl_module --with-http_sub_module'



#echo \"nginx script\"
git clone git://github.com/jnstq/rails-nginx-passenger-ubuntu.git
sudo cp rails-nginx-passenger-ubuntu/nginx/nginx /etc/init.d/nginx
sudo chown root:root /etc/init.d/nginx
sudo cp /home/vagrant/vagrant/nginx_conf.conf /etc/nginx/nginx.conf
 

#cd ../
#rails new r_app
git clone #{git_repo}
/etc/init.d/nginx restart


"

nginx_conf="server {
    listen       80;
    vagrant /home/vagrant/#{git_project}/public;
    passenger_enabled on;
    access_log /var/logs/nginx-access.log;
    error_log  /var/logs/nginx-error.log;

    error_page 500 502 503 504 50x.html;
  }"








begin 
	#raise 'A test exception.'
	FileUtils.mkdir("vagrant_home")
	FileUtils.cd("vagrant_home")
	system 'vagrant init'
	File.new("Vagrantfile", "w+").syswrite(vagrant)
	FileUtils.mkdir("vagrant")
	File.new("vagrant/bootstart.sh", "w+").syswrite(bootstart)
	File.new("vagrant/vagrant_kickstart_conf.sh", "w+").syswrite(vagrant_kickstart_conf)
	File.new("vagrant/nginx_conf.conf", "w+").syswrite(nginx_conf)
	FileUtils.chmod_R(0770, "vagrant")
	system 'vagrant up'
rescue Exception => e  
	  puts e.message  
	  puts e.backtrace.inspect  
end  




#puts Dir.entries(".")
#FileUtils.cd('/', :verbose => true)
